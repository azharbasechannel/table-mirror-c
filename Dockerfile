<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    	    <title>Hello, world!</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  
  <body>
  <div class="container">
<div class="table-responsive-md">
  <table class="table-bordered table-striped table-responsive-stack">
	<h3> Top 10 Team : </h3>
  <thead class="bg-danger">
    <tr>
      <th scope="col">Rank</th>
      <th scope="col">Team</th>
      <th scope="col">Home deface</th>
      <th scope="col">Special Deface</th>
      <th scope="col">Total deface</th>
      <th scope="col">Archived</th>
    </tr>
  </thead>
  <tbody class="table-dark">
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
      <td>Larry</td>
      <td>the Bird</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
      <td>Larry</td>
      <td>the Bird</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
            <td>Larry</td>
      <td>the Bird</td>
    </tr>
        <tr>
      <th scope="row">4</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Larry</td>
      <td>the Bird</td>
    </tr>
            <tr>
      <th scope="row">5</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Larry</td>
      <td>the Bird</td>
    </tr>
            <tr>
      <th scope="row">6</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Larry</td>
      <td>the Bird</td>
    </tr>
            <tr>
      <th scope="row">7</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Larry</td>
      <td>the Bird</td>
    </tr>
            <tr>
      <th scope="row">8</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Larry</td>
      <td>the Bird</td>
    </tr>
            <tr>
      <th scope="row">9</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Larry</td>
      <td>the Bird</td>
    </tr>
            <tr>
      <th scope="row">10</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Larry</td>
      <td>the Bird</td>
    </tr>
  </tbody>
</table>
</div>
<div class="table-responsive-md">
  <table class="table-bordered table-striped table-responsive-stack">
 	<h3> Baru dideface : </h3>
  <thead class="bg-danger">
    <tr>
      <th scope="col">Tanggal</th>
      <th scope="col">Attacker</th>
      <th scope="col">Team</th>
      <th scope="col">Special</th>
      <th scope="col">Home</th>
      <th scope="col">URL</th>
      <th scope="col text-center">WEB IP</th>
      <th scope="col">Mirror</th>
    </tr>
  </thead>
  <tbody class="table-dark">
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label>
</div></td>
      <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
      <td>https://google.com</td>
      <td>127.0.0.1</td>
      <td>https://mirror-c.ga</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
     <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
      <td>https://google.com</td>
      <td>127.0.0.1</td>
      <td>https://mirror-c.ga</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
      <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
      <td>https://google.com</td>
      <td>127.0.0.1</td>
      <td>https://mirror-c.ga</td>
      <td></td>
    </tr>
        <tr>
      <th scope="row">4</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
   <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
      <td>https://google.com</td>
      <td>127.0.0.1</td>
      <td>https://mirror-c.ga</td>
      <td></td>
    </tr>
  </tbody>
</table>
<hr>
</div>
<div class="table-responsive-lg">
 <table class="table-bordered table-striped table-responsive-stack">
 	<h3> Baru dideface archive: </h3>
  <thead class="bg-danger">
    <tr>
      <th scope="col">Tanggal</th>
      <th scope="col">Attacker</th>
      <th scope="col">Team</th>
      <th scope="col">Special</th>
      <th scope="col">Home</th>
      <th scope="col">URL</th>
      <th scope="col text-center">WEB IP</th>
      <th scope="col">Mirror</th>
    </tr>
  </thead>
  <tbody class="table-dark">
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label>
</div></td>
      <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
      <td>https://google.com</td>
      <td>127.0.0.1</td>
      <td>https://mirror-c.ga</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
     <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
      <td>https://google.com</td>
      <td>127.0.0.1</td>
      <td>https://mirror-c.ga</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
      <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
      <td>https://google.com</td>
      <td>127.0.0.1</td>
      <td>https://mirror-c.ga</td>
      <td></td>
    </tr>
        <tr>
      <th scope="row">4</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
   <td><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
  </label></td>
      <td>https://google.com</td>
      <td>127.0.0.1</td>
      <td>https://mirror-c.ga</td>
      <td></td>
    </tr>
  </tbody>
</table>
<hr>
</div>
</div>
</div>
  	
  
  
  
  
  
  
  
  
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>